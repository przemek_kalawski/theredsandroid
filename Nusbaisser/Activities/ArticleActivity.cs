﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using Square.Picasso;

namespace Nusbaisser
{
	[Activity(Label = "Article details")]
	public class ArticleActivity : BaseActivity
	{
		private List<Tuple<int, int>> _tuples = new List<Tuple<int, int>>();
		private List<string> _imageSourceList = new List<string>();

		private ArticleImageAdapter _articleImageAdapter;
		private RecyclerView photosView;
		private RecyclerView.LayoutManager LayoutManager;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			actionBarDrawerToggle.DrawerIndicatorEnabled = false;

			// Create your application here
			LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);

			View contentView = inflater.Inflate(Resource.Layout.article_layout, null, false);
			AndroidDrawerLayout.AddView(contentView, 0);

			string body = Intent.GetStringExtra("value");
			string image = Intent.GetStringExtra("image");

			ImageSourceList(body);
			//remove img src from body
			body = Regex.Replace(body, "<img.+?src=[\"'](.+?)[\"'].*?>", "");
			//body = Regex.Replace(body, "<br>", "");
			body = Regex.Replace(body, @"<span(?: [^>]*)?>", "<span>");
			body = RemoveUnnecessaryTags(body, "(*trescdluga*)");
			ExtractOtherStyleText(body, "span");
			body = RemoveUnnecessaryTags(body, "</span>");
			body = RemoveUnnecessaryTags(body, "<span>");
			TextView contentTextView = FindViewById<TextView>(Resource.Id.article_body);
			SetStyleText(contentTextView, body);

			ImageView articlePhoto = FindViewById<ImageView>(Resource.Id.article_photo);
			Picasso.With(this).Load(new UrlCreator().GetArticleImageUri(image)).Into(articlePhoto);


			LayoutManager = new LinearLayoutManager(this);
			photosView = FindViewById<RecyclerView>(Resource.Id.article_content_image_list);
			photosView.NestedScrollingEnabled = false;
			LoadPhotos();
		}

		private string RemoveUnnecessaryTags(string body, string pattern)
		{
			//remove (*trescdluga*)
			string str = body.Replace(pattern, "");
			//replace text between <span> tags
			//str = ExtractString(body, "span");
			return str;
		}

		private void ExtractOtherStyleText(string s, string tag)
		{
			// You should check for errors in real-world code, omitted for brevity
			var startTag = "<" + tag + ">";
			int startIndex = s.IndexOf(startTag) > 0 ? s.IndexOf(startTag) : 0;
			int endIndex = s.IndexOf("</" + tag + ">", startIndex);
			int oldStartIndex = 0;

			while (startIndex > oldStartIndex && endIndex > startIndex)
			{
				oldStartIndex = startIndex;

				Regex regex1 = new Regex(@"<span>");
				s = regex1.Replace(s, "", 1);
				Regex regex2 = new Regex(@"</span>");
				s = regex2.Replace(s, "", 1);

				_tuples.Add(new Tuple<int, int>(startIndex, endIndex - (tag.Length + 2 /* <> */)));
				startIndex = s.IndexOf(startTag, endIndex- (tag.Length + 2 /* <> */)) > 0 ? s.IndexOf(startTag, endIndex- (tag.Length + 2 /* <> */)) : 0;
				endIndex = s.IndexOf("</" + tag + ">", startIndex);
			}
		}

		private void SetStyleText(TextView content, string body)
		{
			SpannableString str = new SpannableString(body);
			foreach (var tuple in _tuples)
			{
				StyleSpan boldSpan = new StyleSpan(TypefaceStyle.Bold);
				str.SetSpan(boldSpan, tuple.Item1, tuple.Item2, SpanTypes.ExclusiveExclusive);
			}

			content.SetText(str, TextView.BufferType.Spannable);
		}

		private void ImageSourceList(string body)
		{
			var x = Regex.Matches(body, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase);
			foreach (var item in x)
			{
				var c = (Match)item;
				_imageSourceList.Add(c.Groups[1].Value);
			}
		}

		public override void Toolbar_NavigationClick(object sender, Android.Support.V7.Widget.Toolbar.NavigationClickEventArgs e)
		{
			Finish();
		}

		private void LoadPhotos()
		{
			if(_imageSourceList.Count > 0)
			{
				_articleImageAdapter = new ArticleImageAdapter(_imageSourceList, this);
				photosView.SetLayoutManager(LayoutManager);
				photosView.SetAdapter(_articleImageAdapter);
			}
		}
	}
}
