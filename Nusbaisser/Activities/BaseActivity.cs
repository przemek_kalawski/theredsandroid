﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	[Activity(Label = "BaseActivity")]
	public class BaseActivity : AppCompatActivity
	{
		public DrawerLayout AndroidDrawerLayout { get; set; }
		protected ActionBarDrawerToggle actionBarDrawerToggle;
		protected Android.Support.V7.Widget.Toolbar toolbar;
		public NavigationView NavigationView { get; set; }
		protected ListView menuDrawerList;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.base_layout);

			InitInstancesDrawer();
		}

		private void InitInstancesDrawer()
		{
			toolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.toolbar);
			SetSupportActionBar(toolbar);
			AndroidDrawerLayout = (DrawerLayout)FindViewById(Resource.Id.drawer_design_support_layout);

			//menu icon animation //animate into arrow
			actionBarDrawerToggle = new ActionBarDrawerToggle(this, AndroidDrawerLayout, Resource.String.app_name, Resource.String.app_name);
			AndroidDrawerLayout.AddDrawerListener(actionBarDrawerToggle);

			toolbar.NavigationClick += Toolbar_NavigationClick;
			toolbar.SetTitleTextColor(Resource.Color.textColorPrimary);

			NavigationView = FindViewById<NavigationView>(Resource.Id.navigation_drawer);

			View headerView = NavigationView.InflateHeaderView(Resource.Layout.navigation_header);

			SupportActionBar.SetHomeButtonEnabled(true);
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);

			List<MenuIconModel> drawerItems = new List<MenuIconModel>()
			{
				new MenuIconModel("News", Resource.Drawable.ic_menu_news),
				new MenuIconModel("Tabela", Resource.Drawable.ic_table),
				new MenuIconModel("Terminarz", Resource.Drawable.ic_menu_calendar),
				new MenuIconModel("Login", Resource.Drawable.ic_menu_login)
			};

			menuDrawerList = (ListView)headerView.FindViewById(Resource.Id.left_drawer);

			IListAdapter adapter = new DrawerItemCustomAdapter(this, Resource.Layout.list_view_menu_item_row, drawerItems);
			menuDrawerList.Adapter = adapter;
		}


		protected override void OnPostCreate(Bundle savedInstanceState)
		{
			base.OnPostCreate(savedInstanceState);
			actionBarDrawerToggle.SyncState();
		}

		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
			actionBarDrawerToggle.OnConfigurationChanged(newConfig);
		}


		public virtual void Toolbar_NavigationClick(object sender, Android.Support.V7.Widget.Toolbar.NavigationClickEventArgs e)
		{
			AndroidDrawerLayout.OpenDrawer(NavigationView);
		}

		public override void OnBackPressed()
		{
			if (AndroidDrawerLayout.IsDrawerOpen(NavigationView))
			{
				AndroidDrawerLayout.CloseDrawer(NavigationView);
			}
			else
			{
				base.OnBackPressed();
				OverridePendingTransition(Resource.Animation.slide_in_left, Resource.Animation.slide_out_right);
			}
		}
	}
}
