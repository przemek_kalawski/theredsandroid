﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	[Activity(Label = "LoginActivity")]
	public class LoginActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);

			View contentView = inflater.Inflate(Resource.Layout.login_layout, null, false);
			AndroidDrawerLayout.AddView(contentView, 0);
		}
	}
}
