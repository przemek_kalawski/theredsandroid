﻿using Android.App;
using Android.OS;
using Android.Views;
using System.Json;
using Android.Support.V7.Widget;
using Android.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Support.Design.Widget;
using Android.Content;
using System;
using Java.Lang;

namespace Nusbaisser
{
	[Activity(Label = "")]
	public class MainActivity : BaseActivity
	{
		public Articles Articles { get; set; }
		public List<UserModel> Users { get; set; }
		public RecyclerView ArticleList { get; set; }
		public RecyclerView.LayoutManager LayoutManager { get; set; }
		public ArticleListAdapter Adapter { get; set; }

		public ProgressBar ProgressBar { get; set; }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			this.Title = GetString(Resource.String.lfc);

			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);

			View contentView = inflater.Inflate(Resource.Layout.Main, null, false);
			AndroidDrawerLayout.AddView(contentView, 0);

			ArticleList = FindViewById<RecyclerView>(Resource.Id.article_list);
			LayoutManager = new LinearLayoutManager(this);
			ProgressBar = FindViewById<ProgressBar>(Resource.Id.progress_bar);

			ProgressBarAnimation anim = new ProgressBarAnimation(ProgressBar, 0, 360);
			anim.Duration = 1000;
			ProgressBar.Animation = anim;

			LoadArticles();
		}

		private void LoadArticles()
		{
			new ArticleDataLoaderAsync(this).Execute();
		}
	}
}

