﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	[Activity(Label = "MilkyWay", MainLauncher = true)]
	public class SplashActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.splash_screen_layout);

			new Handler().PostDelayed(HandleAction, 3000);
		}

		private void HandleAction()
		{
			Intent intent = new Intent(this, typeof(MainActivity));
			this.StartActivity(intent);
			Finish();
		}
	}
}
