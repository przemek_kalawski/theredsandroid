﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	[Activity(Label = "TableActivity")]
	public class TableActivity : BaseActivity
	{
		ListView teamTableView;
		List<TeamTableModel> teams;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);

			View contentView = inflater.Inflate(Resource.Layout.table_layout, null, false);
			AndroidDrawerLayout.AddView(contentView, 0);

			teamTableView = FindViewById<ListView>(Resource.Id.teams_table_list_view);
			IListAdapter adapter = new TeamItemAdapter(this, Resource.Layout.team_timetable_cell, teams);
			teamTableView.Adapter = adapter;
		}
	}
}
