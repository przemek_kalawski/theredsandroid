﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	[Activity(Label = "TimeTableActivity")]
	public class TimeTableActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);

			View contentView = inflater.Inflate(Resource.Layout.timetable_layout, null, false);
			AndroidDrawerLayout.AddView(contentView, 0);
		}
	}
}
