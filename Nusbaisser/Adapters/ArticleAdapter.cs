﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Square.Picasso;

namespace Nusbaisser
{
	public class ArticleAdapter: BaseAdapter
	{
		ArticleModel _article;
		Context _context;

		public ArticleAdapter(Context context, ArticleModel article)
		{
			_context = context;
			_article = article;
		}

		public override int Count
		{
			get
			{
				return 0;
			}
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return new JavaObjectWrapper<NavItem>() { };
		}

		public override long GetItemId(int position)
		{
			return 0;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view;

			if(convertView == null)
			{
				LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
				view = inflater.Inflate(Resource.Layout.article_layout, null);
			}
			else
			{
				view = convertView;
			}
			return view;
		}
	}
}
