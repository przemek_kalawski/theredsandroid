﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Square.Picasso;

namespace Nusbaisser
{
	public class ArticleImageAdapter: RecyclerView.Adapter
	{
		private List<string> _imageSourceList;
		private Context _context;


		public ArticleImageAdapter(List<string> imageSourceList, Context context)
		{
			_imageSourceList = imageSourceList;
			_context = context;
		}

		public override int ItemCount
		{
			get
			{
				return _imageSourceList.Count;
			}
		}

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			ArticleImageViewHolder myHolder = holder as ArticleImageViewHolder;
			Picasso.With(_context).Load(_imageSourceList[position]).Into(myHolder.PhotoImageView);
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.photo_cell, parent, false);

			ImageView imageView = row.FindViewById<ImageView>(Resource.Id.article_content_image);

			ArticleImageViewHolder view = new ArticleImageViewHolder(row)
			{
				PhotoImageView = imageView
			};

			return view;
		}

		public class ArticleImageViewHolder : RecyclerView.ViewHolder
		{
			public View mainView { get; set; }

			public ImageView PhotoImageView { get; set; }

			public ArticleImageViewHolder(View view): base(view)
			{
				 mainView = view;
			}

		}

	}
}
