﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Square.Picasso;

namespace Nusbaisser
{
	public class ArticleListAdapter : RecyclerView.Adapter
	{
		private List<ArticleModel> _articles;
		private Context _context;
		private IDataService _dataService;
		private List<UserModel> _users;

		public event EventHandler<int> eventHandler;


		public ArticleListAdapter(List<ArticleModel> articles, List<UserModel> users, Context context)
		{
			_articles = articles;
			_context = context;
			_dataService = TinyIoC.TinyIoCContainer.Current.Resolve<DataService>();
			_users = users;
		}

		public class ArticleMainView : RecyclerView.ViewHolder
		{
			public View mainView { get; set; }

			private TextView _authorNameView;

			public TextView AuthorNameView { get; set; }
			public TextView ArticleDateTimeView { get; set; }
			public ImageView ArticlePhoto { get; set; }
			public TextView ArticleContentView { get; set; }


			public ArticleMainView(View view, Action<int> eventHandler) : base(view)
			{
				mainView = view;
				view.Click += (sender, e) => eventHandler(AdapterPosition);
			}
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.article_cell, parent, false);
			TextView articleAuthorName = row.FindViewById<TextView>(Resource.Id.author_name);
			TextView articleDateTime = row.FindViewById<TextView>(Resource.Id.art_dateTime);
			ImageView articlePhoto = row.FindViewById<ImageView>(Resource.Id.art_photo);
			TextView articleContent = row.FindViewById<TextView>(Resource.Id.art_content);


			ArticleMainView view = new ArticleMainView(row, ClickEvent)
			{
				AuthorNameView = articleAuthorName,
				ArticleDateTimeView = articleDateTime,
				ArticlePhoto = articlePhoto,
				ArticleContentView = articleContent
			};
			return view;
		}

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			ArticleMainView myHolder = holder as ArticleMainView;
			myHolder.AuthorNameView.Text = _articles[position].Title;
			myHolder.ArticleContentView.Text = ExtractString(_articles[position].Body) == null ? _articles[position].Title : ExtractString(_articles[position].Body);
			myHolder.ArticleDateTimeView.Text = TimeOfCreation(_articles[position].Created);
			Picasso.With(_context).Load(new UrlCreator().GetArticleImageUri(_articles[position].Image)).Into(myHolder.ArticlePhoto);
		}

		private string ExtractString(string s)
		{
			// You should check for errors in real-world code, omitted for brevity
			var startTag = "";
			int startIndex = s.IndexOf(startTag) + startTag.Length;
			int endIndex = s.IndexOf("(*trescdluga*)");
			if(endIndex > startIndex)
			{
				return s.Substring(startIndex, endIndex - startIndex);
			}
			else
			{
				return null;
			}
		}

		private string TimeOfCreation(DateTime time)
		{
			var t = DateTime.Now - time;
			return TimeFromCreated(t);
		}

		private static string TimeFromCreated(TimeSpan t)
		{
			if (t.Days > 0)
			{
				return t.Days + " day" + Plural(t.Days);
			}
			if (t.Hours > 0)
			{
				return t.Hours + " hour" + Plural(t.Hours);
			}
			if (t.Minutes > 0)
			{
				return t.Minutes + " minute" + Plural(t.Minutes);
			}

			return "below minute";
		}

		private static string Plural(int number)
		{
			return number > 1 ? "s" : "";
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override int ItemCount
		{
			get
			{
				return _articles.Count;
			}
		}

		public void ClickEvent(int position)
		{
			if (eventHandler != null)
			{
				eventHandler(this, position);
			}
			Intent intent = new Intent(_context, typeof(ArticleActivity));
			intent.PutExtra("value", _articles[position].Body);
			intent.PutExtra("userId", _articles[position].UserId);
			intent.PutExtra("image", _articles[position].Image);
			((Activity)_context).StartActivity(intent);
			((Activity)_context).OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_left);
		}
	}
}
