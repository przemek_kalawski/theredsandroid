﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	public class DrawerItemCustomAdapter : ArrayAdapter<MenuIconModel>
	{
		private BaseActivity _context;
		private int _layoutResourceId;
		private List<MenuIconModel> _data;

		public event Action<int> eventHandler;

		public DrawerItemCustomAdapter(BaseActivity context, int layoutResourceId, List<MenuIconModel> data) : base(context, layoutResourceId, data)
		{
			_context = context;
			_layoutResourceId = layoutResourceId;
			_data = data;
		}

		public override long GetItemId(int position)
		{
			//ClickEvent(position);
			return position;
		}

		public override int Count
		{
			get { return _data.Count; }
		}

		public override Android.Views.View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = _data[position];

			var view = convertView;

			if (view == null)
			{
				view = _context.LayoutInflater.Inflate(_layoutResourceId, null);
			}

			ImageView imageViewIcon = (ImageView)view.FindViewById(Resource.Id.imageViewIcon);
			TextView textViewName = (TextView)view.FindViewById(Resource.Id.textViewName);

			MenuIconModel folder = item;

			imageViewIcon.SetImageResource(folder.Icon);
			textViewName.Text = folder.Name;

			view.Click += (sender, e) => ClickEvent(position);

			return view;
		}

		public void ClickEvent(int position)
		{
			_context.AndroidDrawerLayout.CloseDrawer(_context.NavigationView);
			Intent intent = null;
			switch (position)
			{
				case 0:
					intent = new Intent(_context, typeof(MainActivity));
					_context.StartActivityForResult(intent, 0);
					break;
				case 1:
					intent = new Intent(_context, typeof(TableActivity));
					_context.StartActivityForResult(intent, 0);
					break;
				case 2:
					intent = new Intent(_context, typeof(TimeTableActivity));
					_context.StartActivityForResult(intent, 0);
					break;
				case 3:
					intent = new Intent(_context, typeof(LoginActivity));
					_context.StartActivityForResult(intent, 0);
					break;
			}
			_context.OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_left);
		}
	}
}
