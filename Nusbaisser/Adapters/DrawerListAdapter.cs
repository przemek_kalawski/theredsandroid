﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;

namespace Nusbaisser
{
	public class DrawerListAdapter : BaseAdapter
	{
		Context mContext;
		List<NavItem> mNavItems;

		public DrawerListAdapter(Context context, List<NavItem> navItems)
		{
			mContext = context;
			mNavItems = navItems;
		}

		public override int Count
		{
			get
			{
				return mNavItems.Count;
			}
		}
		//hint: https://forums.xamarin.com/discussion/comment/113672/#Comment_113672
		public override Java.Lang.Object GetItem(int position)
		{
			return new JavaObjectWrapper<NavItem>() { Obj = mNavItems[position] };
		}

		public override long GetItemId(int position)
		{
			return 0;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view;

			if (convertView == null)
			{
				LayoutInflater inflater = (Android.Views.LayoutInflater)mContext.GetSystemService(Context.LayoutInflaterService);
				view = inflater.Inflate(Resource.Layout.drawer_item, null);
			}
			else
			{
				view = convertView;
			}

			TextView titleView = (Android.Widget.TextView)view.FindViewById(Resource.Id.title);
			TextView subtitleView = (Android.Widget.TextView)view.FindViewById(Resource.Id.subTitle);
			ImageView iconView = (Android.Widget.ImageView)view.FindViewById(Resource.Id.icon);

			titleView.Text = mNavItems[position].mTitle;
			subtitleView.Text = mNavItems[position].mSubtitle;
			iconView.SetImageResource(mNavItems[position].mIcon);

			return view;

		}
	}

}
