﻿using System.Collections.Generic;
using System.Json;
using Android.OS;
using Android.Views;

namespace Nusbaisser
{
	public class ArticleDataLoaderAsync : AsyncTask
	{
		private MainActivity _callingActivity;
		public Articles _articles;
		public List<UserModel> _users;

		IDataService dataService;


		public ArticleDataLoaderAsync(MainActivity callingActivity)
		{
			_callingActivity = callingActivity;
			dataService = TinyIoC.TinyIoCContainer.Current.Resolve<DataService>();
		}

		protected override void OnPreExecute()
		{
			base.OnPreExecute();
		}

		public void Articles()
		{
			_articles = GetArticles();
			_callingActivity.Articles = _articles;
			_callingActivity.Users = _users;
		}

		public Articles GetArticles()
		{
			Articles articles = null;
			JsonValue x = dataService.GetNewestArticleContent(new UrlCreator().GetPostsUri());
			if (x != null)
			{
				articles = Newtonsoft.Json.JsonConvert.DeserializeObject<Articles>(x.ToString());
				_users = GetArticlesAuthor(articles.Products);
			}
			_articles = articles;
			return articles;
		}

		private List<UserModel> GetArticlesAuthor(List<ArticleModel> articles)
		{
			List<UserModel> users = new List<UserModel>();
			if (articles == null)
				return null;
			foreach (var article in articles)
			{
				UserModel user = new UserModel();
				user.UserId = article.UserId;
				user.AuthorName = GetAuthorOfArticle(article.UserId);
				users.Add(user);
			}
			return users;
		}

		private string GetAuthorOfArticle(string userId)
		{
			AuthorModel authorName;
			JsonValue x = dataService.GetAuthorName(new UrlCreator().GetUserIdUri(userId));
			if (x != null)
			{
				authorName = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorModel>(x.ToString());
				return authorName.Author[0].AuthorName;
			}

			return "";
		}

		protected override void OnPostExecute(Java.Lang.Object result)
		{
			base.OnPostExecute(result);
			_callingActivity.RunOnUiThread(DisplayUI);
		}

		private void DisplayUI()
		{
			ShowArticles();
		}

		public void ShowArticles()
		{
			if (_callingActivity.Articles != null)
				_callingActivity.Adapter = new ArticleListAdapter(_callingActivity.Articles.Products, _callingActivity.Users, _callingActivity);
			_callingActivity.ArticleList.SetLayoutManager(_callingActivity.LayoutManager);

			if (_callingActivity.Articles != null)
			{
				_callingActivity.ArticleList.SetAdapter(_callingActivity.Adapter);
				_callingActivity.ProgressBar.Visibility = ViewStates.Gone;
			}
		}

		protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
		{
			try
			{
				Articles();
			}
			catch (System.Exception ex)
			{ }

			return null;
		}
	}
}