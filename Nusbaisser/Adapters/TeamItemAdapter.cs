﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Widget;

namespace Nusbaisser
{
	public class TeamItemAdapter: ArrayAdapter<TeamTableModel>
	{
		private Activity _context;
		private int _layoutResourceId;
		private List<TeamTableModel> _items;

		public TeamItemAdapter(Activity context, int layoutResourceId, List<TeamTableModel> items): base(context, layoutResourceId, items)
		{
			_context = context;
			_layoutResourceId = layoutResourceId;
			_items = items;
		}

		public override Android.Views.View GetView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			var item = _items[position];

			var view = convertView;

			if(view == null)
			{
				view = _context.LayoutInflater.Inflate(_layoutResourceId, null);
			}

			TextView teamNameTextView = (TextView)view.FindViewById(Resource.Id.team_name_text_view);
			TextView matchesTextView = (TextView)view.FindViewById(Resource.Id.matches_count_text_view);
			TextView goalsTextView = (TextView)view.FindViewById(Resource.Id.goals_text_view);
			TextView pointsTextView = (TextView)view.FindViewById(Resource.Id.points_text_view);

			teamNameTextView.Text = item.TeamName;
			matchesTextView.Text = (item.WinningMatches + item.DrawingMatches + item.LoosingMatches).ToString();
			goalsTextView.Text = (item.PlusGoals - item.MinusGoals).ToString();
			pointsTextView.Text = (item.WinningMatches * 3 + item.DrawingMatches).ToString();

			return view;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override int Count
		{
			get { return _items.Count; }
		}
	}
}
