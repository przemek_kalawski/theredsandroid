﻿using System;
using Android.App;
using Android.Runtime;

namespace Nusbaisser
{
	[Application]
	public class App: Application
	{
		public App(IntPtr handle, JniHandleOwnership transfer): base(handle, transfer)
		{
		}

		public override void OnCreate()
		{
			Initialize();

			base.OnCreate();
		}

		private static void Initialize()
		{
			//TinyIoC.TinyIoCContainer.Current.AutoRegister() also works 
			//but its performance hit on everytime application starts

			var container = TinyIoC.TinyIoCContainer.Current;

			container.Register<IDataService, DataService>();
			container.Register<IInternetConnectionService, InternetConnectionService>();
		}
	}
}
