﻿using System;
using Android.App;
using Android.OS;
using Android.Views;

namespace Nusbaisser
{
	public class PreferencesFragment : Fragment
	{
		public PreferencesFragment()
		{

		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			return inflater.Inflate(Resource.Layout.fragment_preferences, container, false);
		}
	}
}
