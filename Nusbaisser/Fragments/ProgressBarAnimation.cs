﻿using System;
using Android.Views.Animations;
using Android.Widget;

namespace Nusbaisser
{
	public class ProgressBarAnimation: Animation
	{
		private ProgressBar _progressBar;
		private float _from;
		private float _to;

		public ProgressBarAnimation(ProgressBar progressBar, float from, float to)
		{
			_progressBar = progressBar;
			_from = from;
			_to = to;
		}

		protected override void ApplyTransformation(float interpolatedTime, Transformation t)
		{
			base.ApplyTransformation(interpolatedTime, t);
			float value = _from + (_to - _from) * interpolatedTime;
			_progressBar.Progress = (int)value;
		}
	}
}
