﻿using System;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class ArticleModel
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "created")]
		public DateTime Created { get; set; }

		[JsonProperty(PropertyName = "published")]
		public string Published { get; set; }
	
		[JsonProperty(PropertyName = "comment_count")]
		public int CommentsCount { get; set; }

		[JsonProperty(PropertyName = "user_id")]
		public string UserId { get; set; }

		[JsonProperty(PropertyName = "source")]
		public string Source { get; set; }

		[JsonProperty(PropertyName = "image")]
		public string Image { get; set; }

		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }

		[JsonProperty(PropertyName = "body")]
		public string Body { get; set; }

	}
}
