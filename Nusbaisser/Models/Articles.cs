﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class Articles
	{
		[JsonProperty(PropertyName = "products")]
		public List<ArticleModel> Products { get; set; }
	}
}
