﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class AuthorModel
	{
		[JsonProperty(PropertyName = "products")]
		public List<Author> Author { get; set; }
	}

	public class Author
	{
		[JsonProperty(PropertyName ="author")]
		public string AuthorName { get; set; }
	}
}
