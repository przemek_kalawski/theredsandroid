﻿using System;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class MatchModel
	{
		[JsonProperty(PropertyName ="Id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName ="competition_id")]
		public string CompetitionId { get; set; }

		[JsonProperty(PropertyName="datetime")]
		public string MatchDateTime { get; set; }

		[JsonProperty(PropertyName = "home")]
		public bool Home { get; set; }

		[JsonProperty(PropertyName = "rival")]
		public string RivalName { get; set; }

		[JsonProperty(PropertyName = "score")]
		public string Score { get; set; }

		[JsonProperty(PropertyName="goalscorers")]
		public string Goalscorers { get; set; }

	}
}
