﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class Matches
	{
		[JsonProperty(PropertyName = "products")]
		public List<MatchModel> MatchList { get; set; }
	}
}
