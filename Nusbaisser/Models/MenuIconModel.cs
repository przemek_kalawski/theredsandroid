﻿using System;
namespace Nusbaisser
{
	public class MenuIconModel
	{
		public string Name { get; set;}
		public int Icon { get; set; }

		public MenuIconModel (string name, int icon)
		{
			Name = name;
			Icon = icon;
		}
	}
}
