﻿using System;
namespace Nusbaisser
{
	public class NavItem
	{
		public string mTitle { get; set; }
		public string mSubtitle { get; set; }
		public int mIcon { get; set; }


		public NavItem(string title, string subtitle, int icon)
		{
			mTitle = title;
			mSubtitle = subtitle;
			mIcon = icon;
		}
	}
}
