﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class TableModel
	{
		[JsonProperty(PropertyName = "products")]
		public List<TeamTableModel> Teams { get; set; }
	}
}
