﻿using System;
using Newtonsoft.Json;

namespace Nusbaisser
{
	public class TeamTableModel
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string TeamName { get; set;}

		[JsonProperty(PropertyName = "wins")]
		public int WinningMatches { get; set; }

		[JsonProperty(PropertyName = "draws")]
		public int DrawingMatches { get; set; }

		[JsonProperty(PropertyName = "looses")]
		public int LoosingMatches { get; set; }

		[JsonProperty(PropertyName = "plus_goals")]
		public int PlusGoals { get; set; }

		[JsonProperty(PropertyName = "minus_goals")]
		public int MinusGoals { get; set; }
	}
}
