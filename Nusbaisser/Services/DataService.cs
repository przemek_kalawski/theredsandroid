﻿using System;
using System.IO;
using System.Json;
using System.Net;
using System.Threading.Tasks;

namespace Nusbaisser
{
	public class DataService : IDataService
	{
		private IInternetConnectionService internetConnectionService = TinyIoC.TinyIoCContainer.Current.Resolve<InternetConnectionService>();

		public JsonValue GetAuthorName(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
			request.ContentType = "application/json";
			request.Method = "GET";
			if (internetConnectionService.isOnline())
			{

				using (WebResponse response =  request.GetResponse())
				{
					using (Stream stream = response.GetResponseStream())
					{
						JsonValue jsonDoc = JsonValue.Load(stream);
						Console.WriteLine("Response: {0}", jsonDoc);

						return jsonDoc;
					}
				}
			}
			return null;
		}

		public JsonValue GetNewestArticleContent(string url)
		{
			
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
			request.ContentType = "application/json";
			request.Method = "GET";
			if (internetConnectionService.isOnline())
			{

				using (WebResponse response =  request.GetResponse())
				{
					using (Stream stream = response.GetResponseStream())
					{
						JsonValue jsonDoc =  JsonValue.Load(stream);
						Console.WriteLine("Response: {0}", jsonDoc);

						return jsonDoc;
					}
				}
			}
			return null;
		}

		public async Task<JsonValue> GetTable(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
			request.ContentType = "application/json";
			request.Method = "GET";
			if (internetConnectionService.isOnline())
			{
				using (WebResponse response = await request.GetResponseAsync())
				{
					using (Stream stream = response.GetResponseStream())
					{
						JsonValue jsonDoc = await Task.Run(() => JsonValue.Load(stream));

						return jsonDoc;
					}
				}
			}

			return null;
		}

		public async Task<JsonValue> GetTimeTable(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
			request.ContentType = "application/json";
			request.Method = "GET";
			if (internetConnectionService.isOnline())
			{
				using (WebResponse response = await request.GetResponseAsync())
				{
					using (Stream stream = response.GetResponseStream())
					{
						JsonValue jsonDoc = await Task.Run(() => JsonValue.Load(stream));

						return jsonDoc;
					}
				}
			}
			return null;
		}
	}
}
