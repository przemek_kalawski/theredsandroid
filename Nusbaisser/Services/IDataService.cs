﻿using System;
using System.Json;
using System.Threading.Tasks;

namespace Nusbaisser
{
	public interface IDataService
	{
		JsonValue GetNewestArticleContent(string url);
		JsonValue GetAuthorName(string url);
		Task<JsonValue> GetTimeTable(string url);
		Task<JsonValue> GetTable(string url);
	}
}
