﻿using System;
using System.Net;

namespace Nusbaisser
{
	public class InternetConnectionService : IInternetConnectionService
	{
		public bool isOnline()
		{
			try
			{
				using (var client = new WebClient())
				{
					using (var stream = client.OpenRead("http://www.google.com"))
					{
						return true;
					}
				}
			}
			catch
			{
				return false;
			}
		}

	}
}
