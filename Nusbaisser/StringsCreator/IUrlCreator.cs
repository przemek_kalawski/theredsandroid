﻿using System;
using Android.Graphics;

namespace Nusbaisser
{
	public interface IUrlCreator
	{
		string GetUserIdUri(string id);

		string GetPostsUri();

		string GetArticleImageUri(string photoId);
	}
}
