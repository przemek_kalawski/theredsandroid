﻿using System;
using Android.Graphics;

namespace Nusbaisser
{
	public class UrlCreator: IUrlCreator
	{
		private const string _websiteName = "http://www.lfc.pl/app/webroot/";
		private const string _authorUriPart = "get_author.php?user=";
		private const string _postsUriPart = "get_posts.php?call=1&first=0";
		private const string _photoUriPart = "http://lfc.pl/images/posts/";

		public string GetUserIdUri(string id)
		{
			return _websiteName + _authorUriPart + id;
		}

		public string GetPostsUri()
		{
			return _websiteName + _postsUriPart;
		}

		public string GetArticleImageUri(string photoId)
		{
			return _photoUriPart + photoId;
		}
	}
}
